const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()
let contacts = [
    { name: 'John', phoneNumber: '011-1111111' },
     { name: 'Mawin', phoneNumber: '081-0042440' },
     { name: 'Note', phoneNumber: '097-0378729' },
     { name: 'Dream', phoneNumber: '086-8403926'},
     { name: 'Anna', phoneNumber: '092-6521457'}
]

app.use(bodyParser.json())
app.use(cors())

/// TODO: Develop GET /contacts
app.get('/contacts',(req,res) => {
    res.json(contacts)
})


/// TODO: Develop POST /contacts
app.post('/contacts',(req,res) => {
    let newContact = req.body
    contacts.push(newContact)
    res.status(201).json(newContact)
})


app.listen(3000, () => {
    console.log('API Server started at port 3000')
})

